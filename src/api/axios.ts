import axios from "axios";

const axiosInstance = axios.create({
    baseURL: "https://luna.hn.smartosc.com",
});

export default axiosInstance;
